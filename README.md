# Syn-Zeug

A modern toolbox for synthetic biology

## Code Checks

```
cargo test && cargo fmt && cargo clippy -Z unstable-options
```

## Goals

- Automated primer design
- Automated codon optimization
- Assisted bio-brick assembly / catalog exploration

## Tasks

- Create benchmark tests with larger datasets
- Work through the relevant Rosalind problems
- Set up a Svelte front-end and compile to WebAssembly
- Split up things into sensible modules